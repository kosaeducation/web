import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tarea } from '../models/tarea.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class GestiontareasService {

  private baseUrl = 'http://kosaeducation.sytes.net:8080/api/tareas';

  constructor(private clientehttp: HttpClient) { }

  nuevo(tarea: Tarea) {
    return this.clientehttp.post(`${this.baseUrl}/nuevo`, tarea, httpOptions);
  }

  borrar(tarea: Tarea) {
    this.clientehttp.delete(`${this.baseUrl}/eliminar/${tarea.idTarea}`, httpOptions);
  }
}
