import { Injectable, OnChanges } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public logeado = true;

  constructor() { }


  logear() {
    this.logeado = true;
  }

  logout(){
    this.logeado = false;
  }

}
