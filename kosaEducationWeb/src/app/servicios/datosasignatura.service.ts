import { Injectable } from '@angular/core';
import { Asignatura } from '../models/asignatura.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatosasignaturaService {
  asignaturaActual: Asignatura;

  URL: 'http://kosaeducation.sytes.net:8080/api/asignatura/1';

  constructor(public http: HttpClient) { }

  peticionHttp(id) {

    return this.http
      .get('http://kosaeducation.sytes.net:8080/api/asignaturas/' + id);
  }

  peticionTareas(asignatura) {
    return this.http
      .get('http://kosaeducation.sytes.net:8080/api/tareas/asignatura/' + asignatura);
  }

  peticionTarea(id) {
    return this.http
      .get('http://kosaeducation.sytes.net:8080/api/tareas/' + id);
  }
}
