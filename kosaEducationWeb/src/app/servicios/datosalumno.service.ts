import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Student } from '../models/student-model';
@Injectable({
  providedIn: 'root'
})
export class DatosalumnoService {
  usuarioActual: Student;

  URL: 'http://kosaeducation.sytes.net:8080/api/estudiantes/1';

  constructor(public http: HttpClient) { }

  peticionHttpAlumno() {
    return this.http
      .get('http://kosaeducation.sytes.net:8080/api/estudiantes/1');
  }

  recogerAsignaturas() {
    return this.http.get('http://kosaeducation.sytes.net:8080/api/asignaturas/cursogrado/1');
  }

}
