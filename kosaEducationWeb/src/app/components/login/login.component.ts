import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../servicios/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private loginservice: LoginService) {
  }


  ngOnInit() {
    this.loginservice.logout();
  }

  errorFuncionalidad() {
    Swal.fire({
      title: '¿Ya te has olvidado?',
      input: 'email',
      inputPlaceholder: 'Escribe tu correo electrónico para cambiar la clave',
      type: 'warning',

      onAfterClose: () => {
        Swal.fire({
          title: 'Email de reseteo de clave enviado',
          type: 'success',
        });
      }
    });
  }


}
