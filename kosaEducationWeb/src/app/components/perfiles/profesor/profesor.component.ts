import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Student } from 'src/app/models/student-model';
import Swal from 'sweetalert2';
import { DatosalumnoService } from 'src/app/servicios/datosalumno.service';


@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.scss']
})
export class ProfesorComponent implements OnInit {


  usuarioActual: Student;
  cargado = false;

  nombre = 'Óscar Sanz Sánchez'; //nombre y apellidos del alumno, si no lo concatenamos ponemos las 3 variables seguidas
  remitente1 = 'Isaac S.';
  remitente2 = 'Sergio G.';
  remitente3 = 'Marta J.';
  asunto1 = 'Cambio Horario';
  asunto2 = 'Tareas';
  asunto3 = 'Fecha examen';
  fecha1 = 'Hoy';
  fecha2 = '09-09-1998';
  fecha3 = '28-04-1999';
  cursoAlumno = '2º DAM';
  nombreCentro = 'CES Juan Pablo II'; //nombre del centro
  hoy = new Date().toLocaleString();
  primeraClase = '8:00';
  segundaClase = '8:50';
  terceraClase = '9:40';
  cuartaClase = '10:30';
  quintaClase = '11:20';
  sextaClase = '12:10';
  septimaClase = '13:00';
  octavaClase = '13:50';
  asignatura1 = 'PSP';
  asignatura2 = 'Interfaces';
  asignatura3 = 'Empresa';
  aula1 = 'París';
  aula2 = 'Londres';
  numTelefono = 676800892;
  numRetrasos = '1';
  numAusencias = '4';
  numPartes = '0';
  numExpulsiones = '1';
  //correoElectronico = this.usuarioActual;
  password = 'hola';
  tema = 't1';
  tema2 = 't2';
  extraordinaria = 'Ext.';


  constructor(public peticion: DatosalumnoService) {
  
  }

  ngOnInit() { 
   this.peticion.peticionHttpAlumno().subscribe(result => this.procesamientoDeDatos(result));
  }

  procesamientoDeDatos(datos: any) {
    this.usuarioActual = new Student(datos);
    console.log(this.usuarioActual);
  }

  errorFuncionalidad() {
    Swal.fire('¡No mires!', 'Estamos trabajando duro para traerte esta funcionalidad', 'error');
  }
}
