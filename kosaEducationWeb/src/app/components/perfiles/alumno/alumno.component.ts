import { Component, OnInit } from '@angular/core';
import { Student } from '../../../models/student-model'
import { DatosalumnoService } from '../../../servicios/datosalumno.service';
import Swal from 'sweetalert2';
import { LoginService } from '../../../servicios/login.service';
import { trigger, state, style, animate, transition, useAnimation, keyframes } from '@angular/animations';
import { bounce } from 'ng-animate';


@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.scss'],
})
export class AlumnoComponent implements OnInit {

  usuarioActual: Student;
  cargado = false;

  remitente1 = 'Isaac S.';
  remitente2 = 'Sergio G.';
  remitente3 = 'Marta J.';
  asunto1 = 'Cambio Horario';
  asunto2 = 'Tareas';
  asunto3 = 'Fecha examen';
  fecha1 = 'Hoy';
  fecha2 = '09-09-1998';
  fecha3 = '28-04-1999';
  hoy = new Date().toLocaleString();
  primeraClase = '8:00';
  segundaClase = '8:50';
  terceraClase = '9:40';
  cuartaClase = '10:30';
  quintaClase = '11:20';
  sextaClase = '12:10';
  septimaClase = '13:00';
  octavaClase = '13:50';
  asignatura1 = 'PSP';
  asignatura2 = 'Interfaces';
  asignatura3 = 'Empresa';
  aula1 = 'París';
  aula2 = 'Londres';
  numRetrasos = '1';
  numAusencias = '4';
  numPartes = '0';
  numExpulsiones = '1';
  password = 'hola';
  tema = 't1';
  tema2 = 't2';
  extraordinaria = 'Ext.';


  constructor(public peticion: DatosalumnoService, private loginservice: LoginService) {

  }

  ngOnInit() {

    this.loginservice.logear();
    this.peticion.peticionHttpAlumno().subscribe(result => this.procesamientoDeDatos(result));
  }

  procesamientoDeDatos(datos: any) {
    this.usuarioActual = new Student(datos);
    console.log(this.usuarioActual);
  }

  errorFuncionalidad() {
    Swal.fire('¡No mires!', 'Estamos trabajando duro para traerte esta funcionalidad', 'error');
  }

}
