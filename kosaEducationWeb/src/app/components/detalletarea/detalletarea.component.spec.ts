import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalletareaComponent } from './detalletarea.component';

describe('DetalletareaComponent', () => {
  let component: DetalletareaComponent;
  let fixture: ComponentFixture<DetalletareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalletareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalletareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
