import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../../models/asignatura.model';
import { DatosasignaturaService } from '../../servicios/datosasignatura.service';
import { ActivatedRoute } from '@angular/router';
import { Tarea } from '../../models/tarea.model';
import { ProfesorPublico } from '../../models/profesorpublico.model';
import { GestiontareasService } from '../../servicios/gestiontareas.service';

@Component({
  selector: 'app-detalletarea',
  templateUrl: './detalletarea.component.html',
  styleUrls: ['./detalletarea.component.scss']
})
export class DetalletareaComponent implements OnInit {

  asignatura: Asignatura;
  tarea: Tarea;
  profesor: ProfesorPublico;
  constructor(public servicio: DatosasignaturaService, private route: ActivatedRoute, private gestiontareas: GestiontareasService) {
    this.route.paramMap.subscribe(params => {
      this.servicio.peticionTarea(params.get('idtarea')).subscribe(result => this.procesamientoDeDatos(result));
    });
  }

  ngOnInit() {
  }
  procesamientoDeDatos(datos: any) {
    this.tarea = new Tarea(datos);
    console.log(this.tarea);
    this.profesor = this.tarea.asignatura.profesor;
  }

  borrarTarea(tarea: Tarea){
    console.log(tarea);
    this.gestiontareas.borrar(tarea);
  }
}
