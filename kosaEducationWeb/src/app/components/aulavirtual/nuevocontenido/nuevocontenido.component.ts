import { Component, OnInit, Input } from '@angular/core';
import { Asignatura } from 'src/app/models/asignatura.model';
import { ProfesorPublico } from 'src/app/models/profesorpublico.model';
import { Tarea } from 'src/app/models/tarea.model';
import { TareaPost } from 'src/app/models/tareapost.model.';
import { GestiontareasService } from 'src/app/servicios/gestiontareas.service';

@Component({
  selector: 'app-nuevocontenido',
  templateUrl: './nuevocontenido.component.html',
  styleUrls: ['./nuevocontenido.component.scss']
})
export class NuevocontenidoComponent implements OnInit {
  @Input() asignatura: Asignatura;
  profesor: ProfesorPublico;
  nuevatarea: Tarea = new TareaPost();
  nombre = '';
  descripcion : '';

  constructor(private gestionTareas: GestiontareasService) {}

  ngOnInit() {}
  save() {
    this.nuevatarea.nombre = this.nombre;
    this.nuevatarea.descripcion = this.descripcion;
    this.nuevatarea.asignatura = this.asignatura;    
    this.gestionTareas.nuevo(this.nuevatarea).subscribe();
    this.nombre = '';
    this.descripcion = '';
  }
}
