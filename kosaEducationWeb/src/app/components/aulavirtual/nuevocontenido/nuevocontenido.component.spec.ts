import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevocontenidoComponent } from './nuevocontenido.component';

describe('NuevocontenidoComponent', () => {
  let component: NuevocontenidoComponent;
  let fixture: ComponentFixture<NuevocontenidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevocontenidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevocontenidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
