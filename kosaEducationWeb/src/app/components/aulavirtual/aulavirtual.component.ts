import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Asignatura } from 'src/app/models/asignatura.model';
import { ProfesorPublico } from 'src/app/models/profesorpublico.model';
import { DatosasignaturaService } from 'src/app/servicios/datosasignatura.service';
import { Tarea } from 'src/app/models/tarea.model';
import { GestiontareasService } from 'src/app/servicios/gestiontareas.service';
import { TareaPost } from 'src/app/models/tareapost.model.';

@Component({
  selector: 'app-aulavirtual',
  templateUrl: './aulavirtual.component.html',
  styleUrls: ['./aulavirtual.component.scss']
})
export class AulavirtualComponent implements OnInit {

  asignatura: Asignatura;
  tareas: Tarea[] = [];
  profesor: ProfesorPublico;
  nuevatarea: Tarea = new TareaPost();
  nombre = '';


  constructor(private route: ActivatedRoute, public peticion: DatosasignaturaService) {

    this.route.paramMap.subscribe(params => {
      this.peticion.peticionHttp(params.get('id')).subscribe(result => this.procesamientoDeDatos(result));
    });
  }

  ngOnInit() {
  }


  procesamientoDeDatos(datos: any) {
    this.asignatura = new Asignatura(datos);
    this.peticion.asignaturaActual = this.asignatura;
    this.profesor = new ProfesorPublico(datos.profesor);
    this.route.paramMap.subscribe(params => {
      this.peticion.peticionTareas(params.get('id')).subscribe(result => this.procesaTareas(result));
    });
  }

  procesaTareas(resultado: any) {
    this.tareas = [];
    for (const tarea of resultado) {
      if (tarea.asignatura.idAsignatura == this.asignatura.idAsignatura) {
        this.tareas.push(tarea);
      }
    }
    this.tareas.reverse();
  }


  registro1 = 'Examen';
  registro2 = 'Notas';
  registro3 = 'Tarea';
  registro4 = 'Informacion';

  titulo1 = 'Dia 5 de Mayo, martes';
  titulo2 = 'Nota del Examen T1';
  titulo3 = 'Actividades para el 3 de Mayo';
  titulo4 = 'Cambio de horario';

  cuerpo1 = 'Examen de los temas 2 y 3';
  cuerpo2 = 'Has sacado un 8.7';
  cuerpo3 = 'Resolver los 2 problemas propuestos en clase';
  cuerpo4 = 'El dia 9 de Mayo entramos a las 9:00h';


}
