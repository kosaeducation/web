import { Component, OnInit, Input } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { FileUploader } from 'ng2-file-upload';
import { Tarea } from 'src/app/models/tarea.model';

@Component({
  selector: 'app-subida-archivos',
  templateUrl: './subida-archivos.component.html',
  styleUrls: ['./subida-archivos.component.scss']
})
export class SubidaArchivosComponent {

  public uploader: FileUploader = new FileUploader({ url: 'http://kosaeducation.sytes.net:8080/api/subidas' });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  @Input() tarea: Tarea;

  selectedFile: File = null;

  constructor(private http: HttpClient) {
    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
    }

    this.uploader.onBuildItemForm = (item, form) => {
      form.append('tarea', new Blob([JSON.stringify(this.tarea)],
        {
          type: "application/json"
        }));
    };

  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

}
