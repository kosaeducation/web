import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubidaArchivosComponent } from './subida-archivos.component';

describe('SubidaArchivosComponent', () => {
  let component: SubidaArchivosComponent;
  let fixture: ComponentFixture<SubidaArchivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubidaArchivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubidaArchivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
