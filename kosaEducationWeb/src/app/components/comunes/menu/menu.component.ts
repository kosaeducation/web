import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../../../models/asignatura.model';
import { DatosalumnoService } from '../../../servicios/datosalumno.service';
import { LoginService } from '../../../servicios/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  fullHeight = false;
  asignaturas: Asignatura[];

  logeado = false;

  constructor(public peticion: DatosalumnoService, public login: LoginService) {
    this.logeado = this.login.logeado;
  }

  ngOnInit() {
    this.peticion.recogerAsignaturas().subscribe(result => this.procesamientoDeDatos(result));
  }


  procesamientoDeDatos(datos: any) {
    this.asignaturas = datos;
    console.log(this.asignaturas);
  }

}
