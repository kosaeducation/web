import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localeEs from '@angular/common/locales/es';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadModule } from 'ng2-file-upload';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AulavirtualComponent } from './components/aulavirtual/aulavirtual.component';
import { NuevocontenidoComponent } from './components/aulavirtual/nuevocontenido/nuevocontenido.component';
import { MenuComponent } from './components/comunes/menu/menu.component';
import { SubidaArchivosComponent } from './components/comunes/subida-archivos/subida-archivos.component';
import { DetalletareaComponent } from './components/detalletarea/detalletarea.component';
import { LoginComponent } from './components/login/login.component';
import { AlumnoComponent } from './components/perfiles/alumno/alumno.component';
import { ProfesorComponent } from './components/perfiles/profesor/profesor.component';
import { TareasComponent } from './components/aulavirtual/tareas/tareas.component';
registerLocaleData(localeEs, 'es');


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlumnoComponent,
    ProfesorComponent,
    MenuComponent,
    AulavirtualComponent,
    DetalletareaComponent,
    SubidaArchivosComponent,
    NuevocontenidoComponent,
    TareasComponent,

  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    AppRoutingModule,
    FileUploadModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: LOCALE_ID, useValue: 'es'
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
