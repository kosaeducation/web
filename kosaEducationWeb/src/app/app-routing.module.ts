import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlumnoComponent } from './components/perfiles/alumno/alumno.component';
import { AulavirtualComponent } from './components/aulavirtual/aulavirtual.component';
import { DetalletareaComponent } from './components/detalletarea/detalletarea.component';
import { ProfesorComponent } from './components/perfiles/profesor/profesor.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: AlumnoComponent
  },
  {
    path: 'profesor',
    component: ProfesorComponent
  },
  {
    path: 'aula/:id',
    component: AulavirtualComponent
  },
  {
    path: 'aula/:id/tarea/:idtarea',
    component: DetalletareaComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
