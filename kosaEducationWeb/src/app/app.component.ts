import { Component } from '@angular/core';
import { LoginService } from './servicios/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'KoSa Education';

  logeado = false;

  constructor(public login: LoginService) {
    this.logeado = login.logeado;
  }

}
