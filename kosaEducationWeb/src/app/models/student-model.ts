import { Direccion } from './direccion.model';

export class Student {
    public idEstudiante: number;
    public nombre: any;
    public segundonombre: any;
    public primerapellido: any;
    public segundoapellido: any;
    public dni: any;
    public fechaNacimiento: any;
    public genero: any;
    public telefonomovil: any;
    public telefonofijo: any;
    public fecharegistro: any;
    public fechabaja: any;
    public observaciones: any;
    public email: any;
    public direccion: Direccion;
    public cursogrado: any;
    constructor(json: any) {
        this.idEstudiante = json.idEstudiante;
        this.nombre = json.nombre;
        this.segundonombre = json.segundoNombre;
        this.primerapellido = json.primerApellido;
        this.segundoapellido = json.segundoApellido;
        this.dni = json.dni;
        this.fechaNacimiento = json.fechaNacimiento;
        this.genero = json.genero;
        this.telefonomovil = json.telefonoMovil;
        this.telefonofijo = json.telefonoFijo;
        this.fecharegistro = json.fechaRegistro;
        this.fechabaja = json.fechaBaja;
        this.observaciones = json.observaciones;
        this.email = json.email;
        this.direccion = json.direccion;
        this.cursogrado = json.cursogrado;
    }
}