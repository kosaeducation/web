export class Direccion {
  idDireccion;
  calle;
  numero;
  bloque;
  escalera;
  piso;
  puerta;
  codigoPostal;
  ciudad;
  pais;

  constructor(json) {
    this.idDireccion = json.idDireccion;
    this.calle = json.calle;
    this.numero = json.numero;
    this.bloque = json.bloque;
    this.escalera = json.escalera;
    this.piso = json.piso;
    this.puerta = json.puerta;
    this.codigoPostal = json.codigoPostal;
    this.ciudad = json.ciudad;
    this.pais = json.pais;
  }
}
