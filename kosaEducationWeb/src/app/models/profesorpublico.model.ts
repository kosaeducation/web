import { Direccion } from './direccion.model';

export class ProfesorPublico {
    public idStaff: number;
    public nombre: any;
    public segundonombre: any;
    public primerApellido: any;
    public segundoapellido: any;
    public email: any;
    constructor(json: any) {
        this.idStaff = json.idStaff;
        this.nombre = json.nombre;
        this.segundonombre = json.segundoNombre;
        this.primerApellido = json.primerApellido;
        this.segundoapellido = json.segundoApellido;
        this.email = json.email;
    }
}