export class DiasSemana {

    idDiaSemana;
    nombre;
    constructor(json) {
        this.idDiaSemana = json.idDiaSemana;
        this.nombre = json.nombre;
    }
}
