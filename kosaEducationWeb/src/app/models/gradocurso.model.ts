import { Grado } from './grado.model';
import { Curso } from './curso.model';

export class GradoCurso {
    curso: Curso;
    grado: Grado;

    constructor(json) {
        this.curso = json.curso;
        this.grado = json.grado;
    }
}