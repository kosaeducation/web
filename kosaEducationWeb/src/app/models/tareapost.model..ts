import { Asignatura } from './asignatura.model';

export class TareaPost {

    idTarea = 0;
    nombre = '';
    descripcion = '';
    fechaCreacion = '';
    fechaLimite = '';
    asignatura: Asignatura;

    constructor() {
    }
}
