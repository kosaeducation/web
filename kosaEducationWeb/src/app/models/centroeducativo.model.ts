export class CentroEducativo {

    idCentro;
    nombre;
    nif;
    calle;
    numero;
    telefono1;
    telefono2;
    codigoPostal;
    ciudad;
    pais;

    constructor(json) {
        this.idCentro = json.idCentro;
        this.nombre = json.nombre;
        this.nif = json.nif;
        this.calle = json.calle;
        this.numero = json.numero;
        this.telefono1 = json.telefono1;
        this.telefono2 = json.telefono2;
        this.codigoPostal = json.codigoPostal;
        this.ciudad = json.ciudad;
        this.pais = json.pais;
    }
}