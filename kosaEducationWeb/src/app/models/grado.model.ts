export class Grado {
    
    idGrado;
    nombre;
    coordinador;

    constructor(json) {
        this.idGrado = json.idGrado;
        this.nombre = json.nombre;
        this.coordinador = json.coordinador;
    }
}
