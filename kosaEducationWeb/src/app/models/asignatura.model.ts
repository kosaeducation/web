import { GradoCurso } from './gradocurso.model';
import { ProfesorPublico } from './profesorpublico.model';

export class Asignatura {

    idAsignatura;
    nombre;
    cursogrado: GradoCurso;
    profesor: ProfesorPublico;
    constructor(json) {
        this.idAsignatura = json.idAsignatura;
        this.cursogrado = json.cursogrado;
        this.nombre = json.nombre;
        this.profesor = json.profesor;
    }
}
