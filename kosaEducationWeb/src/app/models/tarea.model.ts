import { Asignatura } from './asignatura.model';

export class Tarea {

    idTarea = 0;
    nombre = '';
    descripcion = '';
    fechaCreacion = '';
    fechaLimite = '';
    asignatura: Asignatura;

    constructor(json?, id?) {
        this.idTarea = json.idTarea;
        this.nombre = json.nombre;
        this.descripcion = json.descripcion;
        this.fechaCreacion = json.fechaCreacion;
        this.fechaLimite = json.fechaLimite;
        this.asignatura = json.asignatura;
    };
}
