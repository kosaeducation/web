export class AulaFisica {

    idAulaFisica;
    nombre;

    constructor(json) {
        this.idAulaFisica = json.idAulaFisica;
        this.nombre = json.nombre;
    }
}