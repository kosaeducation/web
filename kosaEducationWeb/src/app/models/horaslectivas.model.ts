export class HoraLectiva {

    idHora;
    nombre;

    constructor(json) {
        this.idHora = json.idHora;
        this.nombre = json.nombre;
    }
}
